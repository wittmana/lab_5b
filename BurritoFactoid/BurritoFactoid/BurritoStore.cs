﻿// Copywrite 2020 - All rights reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// Written by: Joshua Trimm <trimmj@etsu.edu>, Alexander Wittman, Kellye Tolliver, Danial Bishop 3/1/2020
// File Name: BurritoStore.cs

namespace BurritoFactoid
{
    using BurritoFactoid.BurritoClasses;
    using BurritoFactoid.Factory;

    /// <summary>
    /// Defines the <see cref="BurritoStore" />
    /// </summary>
    public class BurritoStore
    {
        #region Fields

        /// <summary>
        /// Defines the _burrito
        /// </summary>
        private Burrito _burrito;

        #endregion

        #region Methods

        /// <summary>
        /// The createBurrito
        /// </summary>
        /// <param name="_burritoFactory">The _burritoFactory<see cref="IBurritoFactory"/></param>
        public void createBurrito(IBurritoFactory _burritoFactory)
        {
            _burrito = _burritoFactory.createBurrito();
        }

        /// <summary>
        /// The display
        /// </summary>
        /// <returns>The <see cref="string"/></returns>
        public string display()
        {
            return _burrito.display();
        }

        #endregion
    }
}
