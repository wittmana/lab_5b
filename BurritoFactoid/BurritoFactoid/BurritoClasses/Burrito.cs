﻿// Copywrite 2020 Joshua Trimm - All rights reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// Written by: Joshua Trimm <trimmj@etsu.edu>, 3/1/2020
// File Name: Burrito.cs

namespace BurritoFactoid.BurritoClasses
{
    using BurritoFactoid.Beans;
    using BurritoFactoid.Cheese;
    using BurritoFactoid.Meat;
    using BurritoFactoid.Rice;
    using BurritoFactoid.Salsa;
    using BurritoFactoid.Tortilla;
    using BurritoFactoid.Veggies;

    /// <summary>
    /// Defines the <see cref="Burrito" />
    /// </summary>
    public abstract class Burrito
    {
        #region Fields

        /// <summary>
        /// Defines the beans
        /// </summary>
        protected IBeans beans;

        /// <summary>
        /// Defines the cheese
        /// </summary>
        protected ICheese cheese;

        /// <summary>
        /// Defines the meat
        /// </summary>
        protected IMeat meat;

        /// <summary>
        /// Defines the rice
        /// </summary>
        protected IRice rice;

        /// <summary>
        /// Defines the salsa
        /// </summary>
        protected ISalsa salsa;

        /// <summary>
        /// Defines the tortilla
        /// </summary>
        protected ITortilla tortilla;

        /// <summary>
        /// Defines the veggies
        /// </summary>
        protected IVeggies veggies;

        #endregion

        #region Methods

        /// <summary>
        /// The display
        /// </summary>
        /// <returns>The <see cref="string"/></returns>
        public abstract string display();

        /// <summary>
        /// The getBeans
        /// </summary>
        /// <returns>The <see cref="IBeans"/></returns>
        public abstract IBeans getBeans();

        /// <summary>
        /// The getCheese
        /// </summary>
        /// <returns>The <see cref="ICheese"/></returns>
        public abstract ICheese getCheese();

        /// <summary>
        /// The getMeat
        /// </summary>
        /// <returns>The <see cref="IMeat"/></returns>
        public abstract IMeat getMeat();

        /// <summary>
        /// The getRice
        /// </summary>
        /// <returns>The <see cref="IRice"/></returns>
        public abstract IRice getRice();

        /// <summary>
        /// The getSalsa
        /// </summary>
        /// <returns>The <see cref="ISalsa"/></returns>
        public abstract ISalsa getSalsa();

        /// <summary>
        /// The getTortilla
        /// </summary>
        /// <returns>The <see cref="ITortilla"/></returns>
        public abstract ITortilla getTortilla();

        /// <summary>
        /// The getVeggies
        /// </summary>
        /// <returns>The <see cref="IVeggies"/></returns>
        public abstract IVeggies getVeggies();

        /// <summary>
        /// The setBeans
        /// </summary>
        /// <param name="beans">The beans<see cref="IBeans"/></param>
        public abstract void setBeans(IBeans beans);

        /// <summary>
        /// The setCheese
        /// </summary>
        /// <param name="cheese">The cheese<see cref="ICheese"/></param>
        public abstract void setCheese(ICheese cheese);

        /// <summary>
        /// The setMeat
        /// </summary>
        /// <param name="meat">The meat<see cref="IMeat"/></param>
        public abstract void setMeat(IMeat meat);

        /// <summary>
        /// The setRice
        /// </summary>
        /// <param name="rice">The rice<see cref="IRice"/></param>
        public abstract void setRice(IRice rice);

        /// <summary>
        /// The setSalsa
        /// </summary>
        /// <param name="salsa">The salsa<see cref="ISalsa"/></param>
        public abstract void setSalsa(ISalsa salsa);

        /// <summary>
        /// The setTortilla
        /// </summary>
        /// <param name="tortilla">The tortilla<see cref="ITortilla"/></param>
        public abstract void setTortilla(ITortilla tortilla);

        /// <summary>
        /// The setVeggies
        /// </summary>
        /// <param name="veggies">The veggies<see cref="IVeggies"/></param>
        public abstract void setVeggies(IVeggies veggies);

        #endregion
    }
}
