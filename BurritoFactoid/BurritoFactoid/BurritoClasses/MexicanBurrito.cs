﻿// Copywrite 2020 - All rights reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// Written by: Joshua Trimm <trimmj@etsu.edu>, Alexander Wittman, Kellye Tolliver, Danial Bishop 3/1/2020
// File Name: MexicanBurrito.cs

namespace BurritoFactoid.BurritoClasses
{
    using BurritoFactoid.Beans;
    using BurritoFactoid.Cheese;
    using BurritoFactoid.Meat;
    using BurritoFactoid.Rice;
    using BurritoFactoid.Salsa;
    using BurritoFactoid.Tortilla;
    using BurritoFactoid.Veggies;

    /// <summary>
    /// Defines the <see cref="MexicanBurrito" />
    /// </summary>
    public class MexicanBurrito : Burrito
    {
        #region Fields

        /// <summary>
        /// Defines the beans
        /// </summary>
        protected IBeans beans;

        /// <summary>
        /// Defines the cheese
        /// </summary>
        protected ICheese cheese;

        /// <summary>
        /// Defines the meat
        /// </summary>
        protected IMeat meat;

        /// <summary>
        /// Defines the rice
        /// </summary>
        protected IRice rice;

        /// <summary>
        /// Defines the salsa
        /// </summary>
        protected ISalsa salsa;

        /// <summary>
        /// Defines the tortilla
        /// </summary>
        protected ITortilla tortilla;

        /// <summary>
        /// Defines the veggies
        /// </summary>
        protected IVeggies veggies;

        #endregion

        #region Methods

        /// <summary>
        /// The display
        /// </summary>
        /// <returns>The <see cref="string"/></returns>
        public override string display()
        {
            return $"Mexican Burrito on {tortilla.display()} with {meat.display()}, {rice.display()}, {beans.display()}, \n" +
                $"{cheese.display()}, {salsa.display()}, and {veggies.display()}.";
        }

        /// <summary>
        /// The getBeans
        /// </summary>
        /// <returns>The <see cref="IBeans"/></returns>
        public override IBeans getBeans()
        {
            return beans;
        }

        /// <summary>
        /// The getCheese
        /// </summary>
        /// <returns>The <see cref="ICheese"/></returns>
        public override ICheese getCheese()
        {
            return cheese;
        }

        /// <summary>
        /// The getMeat
        /// </summary>
        /// <returns>The <see cref="IMeat"/></returns>
        public override IMeat getMeat()
        {
            return meat;
        }

        /// <summary>
        /// The getRice
        /// </summary>
        /// <returns>The <see cref="IRice"/></returns>
        public override IRice getRice()
        {
            return rice;
        }

        /// <summary>
        /// The getSalsa
        /// </summary>
        /// <returns>The <see cref="ISalsa"/></returns>
        public override ISalsa getSalsa()
        {
            return salsa;
        }

        /// <summary>
        /// The getTortilla
        /// </summary>
        /// <returns>The <see cref="ITortilla"/></returns>
        public override ITortilla getTortilla()
        {
            return tortilla;
        }

        /// <summary>
        /// The getVeggies
        /// </summary>
        /// <returns>The <see cref="IVeggies"/></returns>
        public override IVeggies getVeggies()
        {
            return veggies;
        }

        /// <summary>
        /// The setBeans
        /// </summary>
        /// <param name="beans">The beans<see cref="IBeans"/></param>
        public override void setBeans(IBeans beans)
        {
            this.beans = beans;
        }

        /// <summary>
        /// The setCheese
        /// </summary>
        /// <param name="cheese">The cheese<see cref="ICheese"/></param>
        public override void setCheese(ICheese cheese)
        {
            this.cheese = cheese;
        }

        /// <summary>
        /// The setMeat
        /// </summary>
        /// <param name="meat">The meat<see cref="IMeat"/></param>
        public override void setMeat(IMeat meat)
        {
            this.meat = meat;
        }

        /// <summary>
        /// The setRice
        /// </summary>
        /// <param name="rice">The rice<see cref="IRice"/></param>
        public override void setRice(IRice rice)
        {
            this.rice = rice;
        }

        /// <summary>
        /// The setSalsa
        /// </summary>
        /// <param name="salsa">The salsa<see cref="ISalsa"/></param>
        public override void setSalsa(ISalsa salsa)
        {
            this.salsa = salsa;
        }

        /// <summary>
        /// The setTortilla
        /// </summary>
        /// <param name="tortilla">The tortilla<see cref="ITortilla"/></param>
        public override void setTortilla(ITortilla tortilla)
        {
            this.tortilla = tortilla;
        }

        /// <summary>
        /// The setVeggies
        /// </summary>
        /// <param name="veggies">The veggies<see cref="IVeggies"/></param>
        public override void setVeggies(IVeggies veggies)
        {
            this.veggies = veggies;
        }

        #endregion
    }
}
