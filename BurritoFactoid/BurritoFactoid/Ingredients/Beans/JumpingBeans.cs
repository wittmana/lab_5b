﻿// Copywrite 2020 - All rights reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// Written by: Joshua Trimm <trimmj@etsu.edu>, Alexander Wittman, Kellye Tolliver, Danial Bishop 3/1/2020
// File Name: JumpingBeans.cs

namespace BurritoFactoid.Beans
{
    /// <summary>
    /// Defines the <see cref="JumpingBeans" />
    /// </summary>
    public class JumpingBeans : IBeans
    {
        #region Methods

        /// <summary>
        /// The display
        /// </summary>
        /// <returns>The <see cref="string"/></returns>
        public string display()
        {
            return "Jumping Beans";
        }

        #endregion
    }
}
