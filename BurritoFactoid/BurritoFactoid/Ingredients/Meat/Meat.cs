﻿// Copywrite 2020 - All rights reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// Written by: Joshua Trimm <trimmj@etsu.edu>, Alexander Wittman, Kellye Tolliver, Danial Bishop 3/1/2020
// File Name: Meat.cs

namespace BurritoFactoid.Meat
{
    #region Interfaces

    /// <summary>
    /// Defines the <see cref="IMeat" />
    /// </summary>
    public interface IMeat
    {
        #region Methods

        /// <summary>
        /// The display
        /// </summary>
        /// <returns>The <see cref="string"/></returns>
        string display();

        #endregion
    }

    #endregion
}
