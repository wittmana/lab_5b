﻿// Copywrite 2020 - All rights reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// Written by: Joshua Trimm <trimmj@etsu.edu>, Alexander Wittman, Kellye Tolliver, Danial Bishop 3/1/2020
// File Name: ISalsa.cs

namespace BurritoFactoid.Salsa
{
    #region Interfaces

    /// <summary>
    /// Defines the <see cref="ISalsa" />
    /// </summary>
    public interface ISalsa
    {
        #region Methods

        /// <summary>
        /// The display
        /// </summary>
        /// <returns>The <see cref="string"/></returns>
        string display();

        #endregion
    }

    #endregion
}
