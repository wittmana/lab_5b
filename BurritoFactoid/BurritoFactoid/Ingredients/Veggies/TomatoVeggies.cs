﻿// Copywrite 2020 - All rights reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// Written by: Joshua Trimm <trimmj@etsu.edu>, Alexander Wittman, Kellye Tolliver, Danial Bishop 3/1/2020
// File Name: TomatoVeggies.cs

namespace BurritoFactoid.Veggies
{
    /// <summary>
    /// Defines the <see cref="TomatoVeggies" />
    /// </summary>
    public class TomatoVeggies : IVeggies
    {
        #region Methods

        /// <summary>
        /// The display
        /// </summary>
        /// <returns>The <see cref="string"/></returns>
        public string display()
        {
            return "Tomato Veggies";
        }

        #endregion
    }
}
