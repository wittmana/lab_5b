﻿// Copywrite 2020 - All rights reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// Written by: Joshua Trimm <trimmj@etsu.edu>, Alexander Wittman, Kellye Tolliver, Danial Bishop 3/1/2020
// File Name: MozzarellaCheese.cs

namespace BurritoFactoid.Cheese
{
    /// <summary>
    /// Defines the <see cref="MozzarellaCheese" />
    /// </summary>
    public class MozzarellaCheese : ICheese
    {
        #region Methods

        /// <summary>
        /// The display
        /// </summary>
        /// <returns>The <see cref="string"/></returns>
        public string display()
        {
            return "Mozzarella Cheese";
        }

        #endregion
    }
}
