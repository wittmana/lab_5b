﻿// Copywrite 2020 - All rights reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// Written by: Joshua Trimm <trimmj@etsu.edu>, Alexander Wittman, Kellye Tolliver, Danial Bishop 3/1/2020
// File Name: MonterraryCheese.cs

namespace BurritoFactoid.Cheese
{
    /// <summary>
    /// Defines the <see cref="MonterraryCheese" />
    /// </summary>
    public class MonterraryCheese : ICheese
    {
        #region Methods

        /// <summary>
        /// The display
        /// </summary>
        /// <returns>The <see cref="string"/></returns>
        public string display()
        {
            return "Monterrary Cheese";
        }

        #endregion
    }
}
