﻿// Copywrite 2020 - All rights reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// Written by: Joshua Trimm <trimmj@etsu.edu>, Alexander Wittman, Kellye Tolliver, Danial Bishop 3/1/2020
// File Name: Program.cs

namespace BurritoFactoid
{
    using BurritoFactoid.Factory;
    using System;

    /// <summary>
    /// Defines the <see cref="Program" />
    /// </summary>
    internal class Program
    {
        #region Methods

        /// <summary>
        /// The CreateBurrito
        /// </summary>
        /// <param name="burritoType">The burritoType<see cref="string"/></param>
        /// <returns>The <see cref="IBurritoFactory"/></returns>
        public static IBurritoFactory CreateBurrito(string burritoType)
        {
            if (burritoType == "American")
            {
                AmericanBurritoFactory american = new AmericanBurritoFactory();
                return american;
            }
            else if (burritoType == "Mexican")
            {
                MexicanBurritoFactory mexican = new MexicanBurritoFactory();
                return mexican;
            }
            else
            {
                throw new Exception();
            }
        }

        /// <summary>
        /// The Main
        /// </summary>
        /// <param name="args">The args<see cref="string[]"/></param>
        internal static void Main(string[] args)
        {
            Console.WriteLine("What burrito do you want? (American or Mexican)");
            var burritoType = Console.ReadLine();
            IBurritoFactory burritoFactory = CreateBurrito(burritoType);

            BurritoStore burritoCreated = new BurritoStore();
            burritoCreated.createBurrito(burritoFactory);

            Console.WriteLine($"Your burrito has been made:");
            Console.WriteLine($"{burritoCreated.display()}");

            Console.ReadKey();
        }

        #endregion
    }
}
