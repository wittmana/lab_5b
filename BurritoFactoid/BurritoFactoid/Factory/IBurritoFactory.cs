﻿/* Name:        Alexander Wittman, Joshua Trimm, Daniel Bishop, Kellye Tolliver
 * Project:     Lab_5b
 * Group:       5
 * Date:        03/02/2020
 * Class Name:  IBurritoFactory
 * Description: Factory Interface create abstract burritos containing subclasses as ingredients
 */

namespace BurritoFactoid.Factory
{
    using BurritoFactoid.BurritoClasses;

    #region Interfaces

    /// <summary>
    /// Defines the <see cref="IBurritoFactory" />
    /// </summary>
    public interface IBurritoFactory
    {
        #region Methods

        /// <summary>
        /// Description:    Adds beans to the abstract burrito class
        /// Date:           03/01/2020
        /// </summary>
        /// <param name="burrito">The burrito<see cref="Burrito"/></param>
        void addBeans(Burrito burrito);

        /// <summary>
        /// Description:    Adds Cheese to the abstract burrito class
        /// Date:           03/01/2020
        /// </summary>
        /// <param name="burrito">The burrito<see cref="Burrito"/></param>
        void addCheese(Burrito burrito);

        /// <summary>
        /// Adds Meat to the abstract burrito class
        /// </summary>
        /// <param name="burrito">The burrito<see cref="Burrito"/></param>
        void addMeat(Burrito burrito);

        /// <summary>
        /// Description:    Adds Rice to the abstract burrito class
        /// Date:           03/01/2020
        /// </summary>
        /// <param name="burrito">The burrito<see cref="Burrito"/></param>
        void addRice(Burrito burrito);

        /// <summary>
        /// Adds salsa to the abstract burrito class
        /// </summary>
        /// <param name="burrito">The burrito<see cref="Burrito"/></param>
        void addSalsa(Burrito burrito);

        /// <summary>
        /// Description:    Adds a tortilla to the abstract burrito class
        /// Date:           03/01/2020
        /// </summary>
        /// <param name="burrito">The burrito<see cref="Burrito"/></param>
        void addTortilla(Burrito burrito);

        /// <summary>
        /// Adds Veggies to the abstract burrito class
        /// </summary>
        /// <param name="burrito">The burrito<see cref="Burrito"/></param>
        void addVeggies(Burrito burrito);

        /// <summary>
        /// The createBurrito
        /// </summary>
        /// <returns>The <see cref="Burrito"/></returns>
        Burrito createBurrito();

        #endregion
    }

    #endregion
}
