﻿/* Name:        Alexander Wittman, Joshua Trimm, Daniel Bishop, Kellye Tolliver
 * Project:     Lab_5b
 * Group:       5
 * Date:        03/02/2020
 * Class Name:  MexicanBurritoFactory
 * Description: Factory Interface create Mexican burritos containing Chicken, Wheat Tortilla, black beans, brown rice,
 *              tomatoes, mozarrella, mild salsa
 */

namespace BurritoFactoid.Factory
{
    using BurritoFactoid.Beans;
    using BurritoFactoid.BurritoClasses;
    using BurritoFactoid.Cheese;
    using BurritoFactoid.Meat;
    using BurritoFactoid.Rice;
    using BurritoFactoid.Salsa;
    using BurritoFactoid.Tortilla;
    using BurritoFactoid.Veggies;

    /// <summary>
    /// Defines the <see cref="MexicanBurritoFactory" />
    /// </summary>
    public class MexicanBurritoFactory : IBurritoFactory
    {
        #region Methods

        /// <summary>
        /// The addBeans
        /// </summary>
        /// <param name="burrito">The burrito<see cref="Burrito"/></param>
        public void addBeans(Burrito burrito)
        {
            burrito.setBeans(new BlackBeans());
        }

        /// <summary>
        /// The addCheese
        /// </summary>
        /// <param name="burrito">The burrito<see cref="Burrito"/></param>
        public void addCheese(Burrito burrito)
        {
            burrito.setCheese(new MozzarellaCheese());
        }

        /// <summary>
        /// The addMeat
        /// </summary>
        /// <param name="burrito">The burrito<see cref="Burrito"/></param>
        public void addMeat(Burrito burrito)
        {
            burrito.setMeat(new Chicken());
        }

        /// <summary>
        /// The addRice
        /// </summary>
        /// <param name="burrito">The burrito<see cref="Burrito"/></param>
        public void addRice(Burrito burrito)
        {
            burrito.setRice(new BrownRice());
        }

        /// <summary>
        /// The addSalsa
        /// </summary>
        /// <param name="burrito">The burrito<see cref="Burrito"/></param>
        public void addSalsa(Burrito burrito)
        {
            burrito.setSalsa(new MildSalsa());
        }

        /// <summary>
        /// The addTortilla
        /// </summary>
        /// <param name="burrito">The burrito<see cref="Burrito"/></param>
        public void addTortilla(Burrito burrito)
        {
            burrito.setTortilla(new Wheat());
        }

        /// <summary>
        /// The addVeggies
        /// </summary>
        /// <param name="burrito">The burrito<see cref="Burrito"/></param>
        public void addVeggies(Burrito burrito)
        {
            burrito.setVeggies(new TomatoVeggies());
        }

        /// <summary>
        /// The createBurrito
        /// </summary>
        /// <returns>The <see cref="Burrito"/></returns>
        public Burrito createBurrito()
        {
            MexicanBurrito mexicanBurrito = new MexicanBurrito();
            addBeans(mexicanBurrito);
            addCheese(mexicanBurrito);
            addMeat(mexicanBurrito);
            addRice(mexicanBurrito);
            addSalsa(mexicanBurrito);
            addTortilla(mexicanBurrito);
            addVeggies(mexicanBurrito);
            return mexicanBurrito;
        }

        #endregion
    }
}
