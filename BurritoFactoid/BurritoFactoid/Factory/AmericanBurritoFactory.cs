﻿/* Name:        Alexander Wittman, Joshua Trimm, Daniel Bishop, Kellye Tolliver
 * Project:     Lab_5b
 * Group:       5
 * Date:        03/02/2020
 * Class Name:  AmericanBurritoFactory
 * Description: Factory Interface create American burritos containing ground beef, flour tortilla, jumping beans, 
 *              white rice, lettuce, monterray, and spicy salsa
 */

namespace BurritoFactoid.Factory
{
    using BurritoFactoid.Beans;
    using BurritoFactoid.BurritoClasses;
    using BurritoFactoid.Cheese;
    using BurritoFactoid.Meat;
    using BurritoFactoid.Rice;
    using BurritoFactoid.Salsa;
    using BurritoFactoid.Tortilla;
    using BurritoFactoid.Veggies;

    /// <summary>
    /// Defines the <see cref="AmericanBurritoFactory" />
    /// </summary>
    public class AmericanBurritoFactory : IBurritoFactory
    {
        #region Methods

        /// <summary>
        /// The addBeans
        /// </summary>
        /// <param name="burrito">The burrito<see cref="Burrito"/></param>
        public void addBeans(Burrito burrito)
        {
            burrito.setBeans(new JumpingBeans());
        }

        /// <summary>
        /// The addCheese
        /// </summary>
        /// <param name="burrito">The burrito<see cref="Burrito"/></param>
        public void addCheese(Burrito burrito)
        {
            burrito.setCheese(new MonterraryCheese());
        }

        /// <summary>
        /// The addMeat
        /// </summary>
        /// <param name="burrito">The burrito<see cref="Burrito"/></param>
        public void addMeat(Burrito burrito)
        {
            burrito.setMeat(new GroundBeef());
        }

        /// <summary>
        /// The addRice
        /// </summary>
        /// <param name="burrito">The burrito<see cref="Burrito"/></param>
        public void addRice(Burrito burrito)
        {
            burrito.setRice(new WhiteRice());
        }

        /// <summary>
        /// The addSalsa
        /// </summary>
        /// <param name="burrito">The burrito<see cref="Burrito"/></param>
        public void addSalsa(Burrito burrito)
        {
            burrito.setSalsa(new SpicySalsa());
        }

        /// <summary>
        /// The addTortilla
        /// </summary>
        /// <param name="burrito">The burrito<see cref="Burrito"/></param>
        public void addTortilla(Burrito burrito)
        {
            burrito.setTortilla(new Flour());
        }

        /// <summary>
        /// The addVeggies
        /// </summary>
        /// <param name="burrito">The burrito<see cref="Burrito"/></param>
        public void addVeggies(Burrito burrito)
        {
            burrito.setVeggies(new LettuceVeggies());
        }

        /// <summary>
        /// The createBurrito
        /// </summary>
        /// <returns>The <see cref="Burrito"/></returns>
        public Burrito createBurrito()
        {
            AmericanBurrito americanBurrito = new AmericanBurrito();
            addBeans(americanBurrito);
            addCheese(americanBurrito);
            addMeat(americanBurrito);
            addRice(americanBurrito);
            addSalsa(americanBurrito);
            addTortilla(americanBurrito);
            addVeggies(americanBurrito);
            return americanBurrito;
        }

        #endregion
    }
}
